
# merge_xlsx.py

```
usage: merge_xlsx.py [-h] file1 file2 file3 file4

Python script that reads in four input xlsx files, and performs SQL-style inner joins on specific
shared columns to output a new xlsx file (Default 'Final.xlsx').

positional arguments:
  file1       path/to/xlsx file to merge (1 of 4)
  file2       path/to/xlsx file to merge (2 of 4)
  file3       path/to/xlsx file to merge (3 of 4)
  file4       path/to/xlsx file to merge (4 of 4)

optional arguments:
  -h, --help  show this help message and exit
```



## Getting Started

I would recommend to keep the directory structure as follows:

![project_tree](./docs/media/project_tree.png)

A successful merger of the 4 input xlsx files will always be output to the `data` directory as a file named `Final.xlsx`.

The script has permissions pre-set to allow any user to be able to execute it.  The user can place the python script in any directory they choose.  Feel free to reach out to [a.gonzalez979@gmail.com](a.gonzalez979@gmail.com) or visit [stubs.github.io](https://stubs.github.io) for additional customization to the script.


## Python Module Dependencies

`merge_xlsx.py` was developed using python 2.7.  To keep things light-weight, the script only requires a quick `pip install` of the following modules that are not in Python 2.7's standard library. 

* openpyxl (2.4.9)
* pandas (0.20.3)
* xlrd (1.1.0)

```pip install pandas xlrd openpyxl```


## Common Error Messages (and what they mean!)

`merge_xlsx.py` has a few error messages baked in to assist the user to be able to identify any problems. Most problems are going to originate in the 4 input files that are required for this script to successfully run.


A successful merger of the 4 xlsx files will provide no messages (and will produce a file in the data directory named "Final.xlsx").

-------

![normal](./docs/media/normal_call.png)



If a user attempts to pass in any file that is not an xlsx file, the following error message will be output to the command line.

-------

![non_xlsx_error](./docs/media/non_xlsx_error.png)



If a user attempts to pass in xlsx files that are missing required columns to cross-reference, the following error message will be output to the command line.

-------

![no_coloumn_error](./docs/media/no_column_error.png)



## Built With

* pandas (http://pandas.pydata.org) - pandas is an open source, BSD-licensed library providing high-performance, easy-to-use data structures and data analysis tools for the Python programming language.
* Jupyter (https://jupyter.org) - Project Jupyter is a non-profit, open-source project, born out of the IPython Project in 2014 as it evolved to support interactive data science and scientific computing across all programming languages.


## Authors

* **Aaron D. Gonzalez** - *Software Developer* - [stubs.github.io](https://stubs.github.io) // [a.gonzalez979@gmail.com](a.gonzalez979@gmail.com)
