#!/usr/bin/env python
import pandas as pd
import random
import sys


class XlsxMerger:
    """Class reads four input files into DataFrames, and performs SQL-style inner joins
    on specific shared columns to output a new DataFrame to a new xlsx file (Default 'Final.xlsx').

    Keyword arguments:
        in_file1 -- First of four xlsx files to merge.
        in_file2 -- Second of four xlsx files to merge.
        in_file3 -- Third of four xlsx files to merge.
        in_file4 -- Fourth of four xlsx files to merge.
    """
    def __init__(self, in_file1, in_file2, in_file3, in_file4):
        self.IN_FILE1 = in_file1
        self.IN_FILE2 = in_file2
        self.IN_FILE3 = in_file3
        self.IN_FILE4 = in_file4
        self.in_file_list = [self.IN_FILE1, self.IN_FILE2, self.IN_FILE3, self.IN_FILE4]

        # read in command line parameters & normalize all column headers to deal with optional colons and whitespace.
        in_file_dfs = {}
        for i, v in enumerate(self.in_file_list):
            in_file_dfs["input_df{}".format(i + 1)] = [pd.read_excel(v), str(v)]
            in_file_dfs["input_df{}".format(i + 1)][0].columns = [
                col.replace(":", "").strip().upper() for col in in_file_dfs["input_df{}".format(i + 1)][0].columns
            ]

        exported_df = [pd.DataFrame(), ""]
        affected_df = [pd.DataFrame(), ""]
        host_owner_df = [pd.DataFrame(), ""]
        details_df = [pd.DataFrame(), ""]

        cols_dict = {"exported_cols": ['OFFICE/ORG', 'MITIGATIONS', 'SEVERITY VALUE',
                                        'RESOURCES REQUIRED', 'SCHEDULED COMPLETION DATE',
                                        'MILESTONE WITH COMPLETION DATES', 'MILESTONE CHANGES', 'STATUS', 'COMMENTS'],
                    "affected_cols": ['NAME', 'HOST', 'BENCHMARK', 'SCAN TYPE', 'SCAN DATE',
                                        'SCAN RESULT'],
                    "details_cols": ['RULE VERSION', 'GROUP ID', 'GROUP TITLE', 'BENCHMARK',
                                        'DESCRIPTION', 'CHECK CONTENT', 'FIX'],
                    "host_cols": ['HOST', 'GROUP']
                     }

        # check all columns of input files to see if they have necessary cross-referencing columns needed.
        for _, in_file_df_value in in_file_dfs.items():
            is_matched = 0
            for col_key, col_val in cols_dict.items():
                if not col_key == "host_cols":
                    rand_list = random.sample(col_val, (len(col_val) - 1))
                else:
                    rand_list = random.sample(col_val, len(col_val))

                if set(rand_list) <= set([str(col) for col in in_file_df_value[0].columns]):
                    is_matched = 1
                    if col_key == "exported_cols":
                        if "SECURITY CHECKS" not in in_file_df_value[0].columns:
                            print "{} requires column '{}' for cross-referencing".format(in_file_df_value[1], "SECURITY CHECKS")
                            sys.exit()
                        else:
                            exported_df = [in_file_df_value[0].copy(), in_file_df_value[1]]
                    if col_key == "affected_cols":
                        if "RULE ID" not in in_file_df_value[0].columns:
                            print "{} requires column '{}' for cross-referencing".format(in_file_df_value[1], "RULE ID")
                            sys.exit()
                        else:
                            affected_df = [in_file_df_value[0].copy(), in_file_df_value[1]]
                    if col_key == "details_cols":
                        if "ID" not in in_file_df_value[0].columns:
                            print "{} requires column '{}' for cross-referencing".format(in_file_df_value[1], "ID")
                            sys.exit()
                        else:
                            details_df = [in_file_df_value[0].copy(), in_file_df_value[1]]
                    if col_key == "host_cols":
                        if "HOST" not in in_file_df_value[0].columns:
                            print "{} requires column '{}' for cross-referencing".format(in_file_df_value[1], "HOST")
                            sys.exit()
                        else:
                            host_owner_df = [in_file_df_value[0].copy(), in_file_df_value[1]]
            if not is_matched:
                print "{} is missing a variety of expected columns.  Please double check it".format(in_file_df_value[1])
                sys.exit()

        # add new joining column to merging dataframes & strip whitespace of all string/unicode types in cell
        details_df[0]["ID"] = [val.strip() if type(val) == unicode else val for val in details_df[0]["ID"].values]
        exported_df[0]["ID"] = [val.strip() if type(val) == unicode else val for val in exported_df[0]["SECURITY CHECKS"].values]
        affected_df[0]["ID"] = [val.strip() if type(val) == unicode else val for val in affected_df[0]["RULE ID"].values]
        affected_df[0]["HOST"] = [val.strip() if type(val) == unicode else val for val in affected_df[0]["HOST"].values]
        host_owner_df[0]["HOST"] = [val.strip() if type(val) == unicode else val for val in host_owner_df[0]["HOST"].values]

        # merge the dataframes to make one master dataframe
        affected_host_owner_df = affected_df[0].merge(host_owner_df[0], how="inner", on="HOST")
        new_master_df = details_df[0].merge(exported_df[0], how='inner', on='ID')

        # remove the details file's 'Benchmark' column to avoid collision with affected file's 'Benchmark' column.
        cols_to_use = new_master_df.columns.difference(
            [col for col in affected_host_owner_df.columns if col not in ["ID"]])
        new_master_df = pd.merge(affected_host_owner_df, new_master_df[cols_to_use], how="inner", on="ID")

        # select only necessary columns for the final output
        final_df = new_master_df[["SECURITY CHECKS",
                                   "BENCHMARK",
                                   "DESCRIPTION",
                                   "CHECK CONTENT",
                                   "FIX",
                                   "SCHEDULED COMPLETION DATE",
                                   "HOST",
                                   "GROUP"
        ]]

        # fill NaN null values in dataframe & output to final.xlsx
        final_df = final_df.fillna("None listed")
        final_df.to_excel("data/Final.xlsx")
        # final_df.to_csv("data/Final.csv")   # debug without xlsx readers


def main():
    import argparse
    import os

    parser = argparse.ArgumentParser(description="""
    Read four input xlsx files, and perform 
    SQL-style inner joins on specific shared columns to output 
    a new xlsx file (Default 'Final.xlsx').""")

    def file_choices(fname, choices=["xlsx"]):
        """Function checks to make sure that the files a user passes in via
        command line parameters match a specific file type. Returns a parser error
        if input file type is not of an accepted file type.

        Keyword Arguments:
            fname = path/to/file that needs to be checked.
            choices = list of accepted file types to check against. (Default ["xlsx"])
        """
        ext = os.path.splitext(fname)[1][1:]
        if ext not in choices:
            if len(choices) > 1:
                parser.error("Input file '{}' is not an accepted file type of {}.".format(fname, choices))
            else:
                parser.error("Input file '{}' is not an accepted file type of {}.".format(fname, choices[0]))
        return fname

    # only allow four input files.
    for i in range(4):
        parser.add_argument("file{}".format(i + 1)
                            , type=lambda fn: file_choices(fn)
                            , help="path/to/xlsx file to merge ({} of 4)".format(i + 1)
        )
    args = parser.parse_args()
    XlsxMerger(args.file1, args.file2, args.file3, args.file4)


if __name__ == "__main__":
    try:
        main()
    except Exception, e:
        print "ERROR: {}".format(e)
