# Code Walkthrough: merge_xlsx.py

This document will presenting code snippets from the python script, and explaining
the work flow to (hopefully) yield a greater comprehension of the inner workings of
script.  The code snippets will be copied from the python script in the order in which
they appear in the script (top-down order....NOT necessarily the order in which the code
is executed.

```python
#!/usr/bin/env python
import pandas as pd
import sys
```

The first line `#!/usr/bin/env python` allows the script to be called from the command line
by either typing `python merger_xlsx.py` or by simply typing `./merger_xlsx.py`.

The remaining two lines simply import the additional python packages that this script is going
to depend on.

---

```python
class XlsxMerger:
    """Class reads four input files into DataFrames, and performs SQL-style inner joins
    on specific shared columns to output a new DataFrame to a new xlsx file (Default 'Final.xlsx').

    Keyword arguments:
        in_file1 -- First of four xlsx files to merge.
        in_file2 -- Second of four xlsx files to merge.
        in_file3 -- Third of four xlsx files to merge.
        in_file4 -- Fourth of four xlsx files to merge.
    """
    def __init__(self, in_file1, in_file2, in_file3, in_file4):
        self.IN_FILE1 = in_file1
        self.IN_FILE2 = in_file2
        self.IN_FILE3 = in_file3
        self.IN_FILE4 = in_file4
        self.in_file_list = [self.IN_FILE1, self.IN_FILE2, self.IN_FILE3, self.IN_FILE4]
```

This code snippet showcases the class definition for the custom class XlsxMerger. As one can see
from the `def __init__(...)` function definition, the class requires 4 parameters to be passed in
(for this walkthrough I am going to ignore the `self` references). The 4 passed in `in_file` parameters
are then assigned to new variables that will be used in the class' scope.  Lastly, we pass all of these
new variables into a python list object named `self.in_file_list`.

---

```python
        # read in command line parameters & normalize all column headers to deal with optional colons and whitespace.
        in_file_dfs = {}
        for i, v in enumerate(self.in_file_list):
            in_file_dfs["input_df{}".format(i + 1)] = [pd.read_excel(v), str(v)]
            in_file_dfs["input_df{}".format(i + 1)][0].columns = [
                col.replace(":", "").strip().upper() for col in in_file_dfs["input_df{}".format(i + 1)][0].columns
            ]
```

The commented line atop of the code snippet above offers a general idea of what this code's purpose serves.
The script creates an empty python dictionary object named `in_file_dfs`.  The code iterates through all
the items in the `self.in_file_list` and adds it into the newly created python dictionary by dynamically
generating keys based on the item's index + 1 in `self.in_file_list`.  The value assigned to the dictionary
key is a list whose first item is a pandas Dataframe object that contains the data in the xlsx file; the
second object is just the string representation of the name of the file.

For the pandas Dataframe item in the list, the script examines the column headers for a colon (since some
columns in the test data had some randomly).  If it finds a colon, it removes it.  Next `.strip()`, removes
any extra spaces at the start or end of the column name, and lastly `.upper()` uppercases all the column titles
because column names need to be exactly the same in order to cross-reference (or join) on them.  For example,
"Id" would not be able to join on "ID".

---

```python
        exported_df = [pd.DataFrame(), ""]
        affected_df = [pd.DataFrame(), ""]
        host_owner_df = [pd.DataFrame(), ""]
        details_df = [pd.DataFrame(), ""]

        cols_dict = {"exported_cols": ['OFFICE/ORG', 'MITIGATIONS', 'SEVERITY VALUE',
                                'RESOURCES REQUIRED', 'SCHEDULED COMPLETION DATE',
                                'MILESTONE WITH COMPLETION DATES', 'MILESTONE CHANGES', 'STATUS', 'COMMENTS'],
            "affected_cols": ['NAME', 'HOST', 'BENCHMARK', 'SCAN TYPE', 'SCAN DATE',
                                'SCAN RESULT'],
            "details_cols": ['RULE VERSION', 'GROUP ID', 'GROUP TITLE', 'BENCHMARK',
                                'DESCRIPTION', 'CHECK CONTENT', 'FIX'],
            "host_cols": ['HOST', 'GROUP']
             }
```

Four new variables are instantiated at the top of this code snippet.  When they are created they each are
assigned a list with two items: an empty pandas Dataframe, and an empty string.
Next, a python dictionary containing all the expected columns for the different types of files (as outlined in the
project description that informed the development of this script) is defined. This dictionary is going to be used by
the python script to be able to figure out which input file is which type. This step is necessary because the user
can name files however they want and may attempt to pass them in to be merged.

---

```python
        # check all columns of input files to see if they have necessary cross-referencing columns needed.
        for _, in_file_df_value in in_file_dfs.items():
            is_matched = 0
            for col_key, col_val in cols_dict.items():
                if not col_key == "host_cols":
                    rand_list = random.sample(col_val, (len(col_val) - 1))
                else:
                    rand_list = random.sample(col_val, len(col_val))
```

Next, the program iterates through previously detailed `in_file_dfs` dictionary object and instantiates a
state variable called `is_matched` that is assigned the falsy value of `0`.  For each of the input files,
the program will collect a random sample of known column headers to see if the input file will match against.

---

```python
                if set(rand_list) <= set([str(col) for col in in_file_df_value[0].columns]):
                    is_matched = 1
                    if col_key == "exported_cols":
                        if "SECURITY CHECKS" not in in_file_df_value[0].columns:
                            print "{} requires column '{}' for cross-referencing".format(in_file_df_value[1], "SECURITY CHECKS")
                            sys.exit()
                        else:
                            exported_df = [in_file_df_value[0].copy(), in_file_df_value[1]]
                    if col_key == "affected_cols":
                        if "RULE ID" not in in_file_df_value[0].columns:
                            print "{} requires column '{}' for cross-referencing".format(in_file_df_value[1], "RULE ID")
                            sys.exit()
                        else:
                            affected_df = [in_file_df_value[0].copy(), in_file_df_value[1]]
                    if col_key == "details_cols":
                        if "ID" not in in_file_df_value[0].columns:
                            print "{} requires column '{}' for cross-referencing".format(in_file_df_value[1], "ID")
                            sys.exit()
                        else:
                            details_df = [in_file_df_value[0].copy(), in_file_df_value[1]]
                    if col_key == "host_cols":
                        if "HOST" not in in_file_df_value[0].columns:
                            print "{} requires column '{}' for cross-referencing".format(in_file_df_value[1], "HOST")
                            sys.exit()
                        else:
                            host_owner_df = [in_file_df_value[0].copy(), in_file_df_value[1]]
            if not is_matched:
                print "{} is missing a variety of expected columns.  Please double check it".format(in_file_df_value[1])
                sys.exit()
```

If the passed in Dataframe columns matches against a known column set, the `is_matched` variable is set to `1`.
Next the program checks to see which file type columns matched against the input file. Based off the type of file
type that matched, the program performs one more check to make sure the specific cross-referencing column for that
file type is in the input file.  If it is, the program assigns the input file to a more appropriate variable name
for the remainder of the script.  If the cross-referencing column is not present, then the program outputs an error
message letting the user know that the specific file is missing the column needed for merging.

 **Note:** If one of the input Dataframes is missing a lot of expected columns, the program will struggle to find which
 type of file the input one is truely supposed to be.  When this occurs the output error message returns to a less detailed
 message letting the user know multiple columns are missing in the input file.

---

```python
        # add new joining column to merging dataframes & strip whitespace of all string/unicode types in cell
        details_df[0]["ID"] = [val.strip() if type(val) == unicode else val for val in details_df[0]["ID"].values]
        exported_df[0]["ID"] = [val.strip() if type(val) == unicode else val for val in exported_df[0]["SECURITY CHECKS"].values]
        affected_df[0]["ID"] = [val.strip() if type(val) == unicode else val for val in affected_df[0]["RULE ID"].values]
        affected_df[0]["HOST"] =[val.strip() if type(val) == unicode else val for val in affected_df[0]["HOST"].values]
        host_owner_df[0]["HOST"] =[val.strip() if type(val) == unicode else val for val in host_owner_df[0]["HOST"].values]
```

Once the random Dataframes have been assigned to their correct variable names, we can begin the process to merge them together
on their cross-referencing column values.  pandas cross-references based off values in columns with the exact same name.  With this
in mind the script adds a new column named "ID" to the Exported_from_online.xlsx and Affected_Assets.xlsx files so that they have a
column that matches the "ID" column in the Details.xlsx file.  The values for the new "ID" column are the same values under the original
cross-referencing column name.  If the value is a unicode type (containing both alpha and numeric characters) we strip all whitespace before
and after the value. This same process of cleaning column values is repeated for the "HOST" column that is going to be used to cross-reference
between the Affected_Assets.xlsx file and the Host_Owner.xlsx file.

----

```python
        # merge the dataframes to make one master dataframe
        affected_host_owner_df = affected_df[0].merge(host_owner_df[0], how="inner", on="HOST")
        new_master_df = details_df[0].merge(exported_df[0], how='inner', on='ID')
```

The `affected_df` is merged together with the `host_owner_df` on the cross-referencing column "HOST" to create a larger
dataframe named `affected_host_owners_df`.
The newly cleaned merging/joining columns created in the previous code snippet are then used to actually bring `details_df` and `exported_df` together
to create a larger `new_master_df`

---

```python
        # remove the details file's 'Benchmark' column to avoid collision with affected file's 'Benchmark' column.
        cols_to_use = new_master_df.columns.difference(
            [col for col in affected_host_owner_df.columns if col not in ["ID"]])
        new_master_df = pd.merge(affected_host_owner_df, new_master_df[cols_to_use], how="inner", on="ID")
```

Before we can merge in the two new larger Dataframes `affected_host_owners_df` and `new_master_df` together the code
handles columns that are in both of the two.  The only shared columns in both of the two larger Dataframes are "BENCHMARK"
and "ID".  The job requirement listed outputting the "BENCHMARK" column from the Affected_Asset.xlsx file, and to this end
we have removed the "BENCHMARK" column from the `new_master_df` before merging the two together.

---

```python
        # select only necessary columns for the final output
        final_df = new_master_df[["SECURITY CHECKS",
                                   "BENCHMARK",
                                   "DESCRIPTION",
                                   "CHECK CONTENT",
                                   "FIX",
                                   "SCHEDULED COMPLETION DATE",
                                   "HOST",
                                   "GROUP"
        ]]
```

The python script then simply chooses the specific columns needed to be presented in the final output xlsx file and saves them
as a Dataframe object under the variable named `final_df`.

---

```python
        # fill NaN null values in dataframe & output to final.xlsx
        final_df = final_df.fillna("None listed")
        final_df.to_excel("data/Final.xlsx")
        # final_df.to_csv("data/Final.csv")   # debug without xlsx readers
```

Prior to output to an xlsx file, we fill all empty/NULL values with the string "None listed".  This is the end of the
xlsxMerger custom class.

---

```python
def main():
    import argparse
    import os

    parser = argparse.ArgumentParser(description="""
    Read four input xlsx files, and perform
    SQL-style inner joins on specific shared columns to output
    a new xlsx file (Default 'Final.xlsx').""")

    def file_choices(fname, choices=["xlsx"]):
        """Function checks to make sure that the files a user passes in via
        command line parameters match a specific file type. Returns a parser error
        if input file type is not of an accepted file type.

        Keyword Arguments:
            fname = path/to/file that needs to be checked.
            choices = list of accepted file types to check against. (Default ["xlsx"])
        """
        ext = os.path.splitext(fname)[1][1:]
        if ext not in choices:
            if len(choices) > 1:
                parser.error("Input file '{}' is not an accepted file type of {}.".format(fname, choices))
            else:
                parser.error("Input file '{}' is not an accepted file type of {}.".format(fname, choices[0]))
        return fname

    # only allow four input files.
    for i in range(4):
        parser.add_argument("file{}".format(i + 1)
                            , type=lambda fn: file_choices(fn)
                            , help="path/to/xlsx file to merge ({} of 4)".format(i + 1)
        )
    args = parser.parse_args()
    XlsxMerger(args.file1, args.file2, args.file3, args.file4)
```

This code snippet will only run if the python program is called directly via the command line, if it is imported into
other python scripts this defined `main()` function and the code in its scope will not be ran automatically.

This code handles checking the 4 input files that the user attempts to pass into the merger_xlsx.py script and will stop
the script from even beginning if the input files are not "xlsx" file types.

---

```python
if __name__ == "__main__":
    try:
        main()
    except Exception, e:
        print "ERROR: {}".format(e)
```

This code snippet will only run if the python program is called directly via the command line. It will call the `main()`
function defined in the previous snippet and print out any error messages that cause the script to stop.
